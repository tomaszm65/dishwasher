#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>
#include <cstdlib>
#include <time.h>
#include <windows.h>
using namespace std;

class program{
    private:
        int wtemp, energy, length;
		string name;
	public:
		program(string,int,int,int);
        ~program(){}

        void menu() const
            {
                system("cls");
                cout << "current program: " << name << endl;
                cout << "water temp: " << wtemp << endl;
                cout << "energy consumption: " << energy << endl;
                cout << "program duration: " << length << endl;
                cout << "[ - turn left     ] - turn right        ENTER - pick program      esc - quit" << endl;
            }
        int GetLength() const
        {
            return length;
        }
	};
	
	program::program(string program_name, int water_temp, int energy_consumption, int program_length){
	    name = program_name;
	    wtemp = water_temp;
	    energy = energy_consumption;
	    length = program_length;
	}

int filecreation(string a, string filename, int number, int b, int c, int d){
    ofstream f;
    filename=filename+".txt";
    f.open(filename.c_str());
    f << a << endl;
    f << number << endl;
    f << b << endl;
    f << c << endl;
    f << d << endl;
    f.close();
}

int timer(int length){
    clock_t start;
    clock_t endt;
    double duration;
    double time = 0;
    int timeins, timeinsp;
    int state;
    int key;
        while(time<length){
                if (kbhit()){
                    key = getch();
                    if (key == 61){
                        state = 1;
                    }
                    else if (key == 112){
                        state = 2;
                        cout << "paused, time left: " << length - timeins << endl;
                    }
                    else if (key == 111){
                        return 0;
                    }
                }

                switch(state){
                    case 1:
                        {
                            start = clock();
                            Sleep(1);
                            endt = clock();
                            duration = ( endt - start ) / (double) CLOCKS_PER_SEC;
                            time = time + duration;
                            timeins = time;
                            if(timeins != timeinsp){
                                cout << timeins << " / " << length << endl;
                            }
                            timeinsp = timeins;
                            break;
                        }

                    case 2:
                        {
                            break;
                        }
                    }
            }
            cout << "Program finished" <<endl;
            Sleep(1000);
}

main(){
    string program1_name;
    int program1_number, program1_wtemp, program1_energy, program1_length;
    string program2_name;
    int program2_number, program2_wtemp, program2_energy, program2_length;
    string program3_name;
    int program3_number, program3_wtemp, program3_energy, program3_length;
    string program4_name;
    int program4_number, program4_wtemp, program4_energy, program4_length;
    string program5_name;
    int program5_number, program5_wtemp, program5_energy, program5_length;

        filecreation("program1", "program1_file", 1, 85, 200, 5);
        filecreation("program2", "program2_file", 2, 65, 180, 8);
        filecreation("program3", "program3_file", 3, 45, 125, 12);
        filecreation("program4", "program4_file", 4, 30, 95, 18);

    ifstream f;
    f.open("program1_file.txt");
        f >> program1_name >> program1_number >> program1_wtemp >> program1_energy >> program1_length;
    f.close();

    f.open("program2_file.txt");
        f >> program2_name >> program2_number >> program2_wtemp >> program2_energy >> program2_length;
    f.close();

    f.open("program3_file.txt");
        f >> program3_name >> program3_number >> program3_wtemp >> program3_energy >>program3_length;
    f.close();

    f.open("program4_file.txt");
        f >> program4_name >> program4_number >> program4_wtemp >> program4_energy >> program4_length;
    f.close();

    f.open("last_used.txt");
        if(f.good() != true){
            filecreation(program1_name, "last_used", program1_number, program1_wtemp, program1_energy, program1_length);
            cout << "creating file last_used:restart program" << endl;
        }
        f >> program5_name >> program5_number >> program5_wtemp >> program5_energy >> program5_length;
    f.close();

    program program1(program1_name, program1_wtemp, program1_energy, program1_length);
    program program2(program2_name, program2_wtemp, program2_energy, program2_length);
    program program3(program3_name, program3_wtemp, program3_energy, program3_length);
    program program4(program4_name, program4_wtemp, program4_energy, program4_length);
    program program5(program5_name, program5_wtemp, program5_energy, program5_length);

    int key;
    int i = 1;
    int length;
    i = program5_number;
    while(key != 27){
        switch(i){
            case 1:
                program1.menu();
                length = program1.GetLength();
                filecreation(program1_name, "last_used", program1_number, program1_wtemp, program1_energy, program1_length);
                break;
            case 2:
                program2.menu();
                length = program2.GetLength();
                filecreation(program2_name, "last_used", program2_number, program2_wtemp, program2_energy, program2_length);
                break;
            case 3:
                program3.menu();
                length = program3.GetLength();
                filecreation(program3_name, "last_used", program3_number, program3_wtemp, program3_energy, program3_length);
                break; 
            case 4:
                program4.menu();
                length = program4.GetLength();
                filecreation(program4_name, "last_used", program4_number, program4_wtemp, program4_energy, program4_length);
                break;
            }
        key = getch();
        if (key == 91){
            i--;
            if(i == 0){
                i = 4;
            }
        }
        else if(key == 93){
            i++;
             if(i == 5){
                i = 1;
            }
        }
        else if(key == 13){
            cout << "o - abaddon prgram     = - start program     p - pause program" << endl;
            timer(length);
        }
    }
	return 0;
}
